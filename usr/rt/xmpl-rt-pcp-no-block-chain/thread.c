/** \file
 *  \brief Example application using threads - a more complex 
 *         example using synchronisation
 */

/*
 * Copyright (c) 2010, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Haldeneggsteig 4, CH-8092 Zurich. Attn: Systems Group.
 */

#include <stdio.h>
#include <string.h>

#include <barrelfish/barrelfish.h>
#include <bench/bench.h>

#include <barrelfish/threads.h>

#define ANSI_COLOR_RED      "\x1b[31m"
#define ANSI_COLOR_GREEN    "\x1b[32m"
#define ANSI_COLOR_YELLOW   "\x1b[33m"
#define ANSI_COLOR_RESET    "\x1b[0m"

// data to pass to each thread
struct rt_thread_data {
    int t_num;
    struct rt_thread_param rtp;
    struct thread *t_id;
    int *start_cntr;
    struct thread_sem *sem;
    int ret;
};

unsigned long SHORT_DELAY  = 2000000;
unsigned long COMMON_DELAY = 8000000;
unsigned long LONG_DELAY   = 10000000;

unsigned long critical_zone_data_a = 0;
unsigned long critical_zone_data_b = 0;
struct thread_pcp_bi_sem rt_sem_a;
struct thread_pcp_bi_sem rt_sem_b;

static void rt_thread_0 (struct rt_thread_data *t_data)
{
    while (1) {
        printf(ANSI_COLOR_GREEN "========rt thread %d start========" ANSI_COLOR_RESET "\n", t_data->rtp.id);

        thread_pcp_bi_sem_wait(&rt_sem_a);
        critical_zone_data_a ++;
        for(volatile unsigned long i = 0; i < COMMON_DELAY; ++ i);
        thread_pcp_bi_sem_signal(&rt_sem_a);

        printf(ANSI_COLOR_GREEN "========rt thread %d end=========" ANSI_COLOR_RESET "\n\n", t_data->rtp.id);
        thread_yield();
    }
}

static void rt_thread_1 (struct rt_thread_data *t_data)
{
    while (1) {
        printf(ANSI_COLOR_GREEN "========rt thread %d start========" ANSI_COLOR_RESET "\n", t_data->rtp.id);

        thread_pcp_bi_sem_wait(&rt_sem_a);
        critical_zone_data_a ++;
        for(volatile unsigned long i = 0; i < SHORT_DELAY; ++ i);
        thread_pcp_bi_sem_wait(&rt_sem_b);
        critical_zone_data_b ++;
        for(volatile unsigned long i = 0; i < COMMON_DELAY; ++ i);
        for(volatile unsigned long i = 0; i < SHORT_DELAY; ++ i);
        thread_pcp_bi_sem_signal(&rt_sem_b);
        // for(volatile unsigned long i = 0; i < SHORT_DELAY; ++ i);
        thread_pcp_bi_sem_signal(&rt_sem_a);

        printf(ANSI_COLOR_GREEN "========rt thread %d end=========" ANSI_COLOR_RESET "\n\n", t_data->rtp.id);
        thread_yield();
    }
}

static void rt_thread_2 (struct rt_thread_data *t_data)
{
    while (1) {
        printf(ANSI_COLOR_GREEN "========rt thread %d start========" ANSI_COLOR_RESET "\n", t_data->rtp.id);

        //unsigned long j = 0;
        //for(volatile unsigned long i = 0; i < LONG_COMMON_DELAY; ++ i) {
        //    j += i;
        //}
        thread_pcp_bi_sem_wait(&rt_sem_b);
        critical_zone_data_b ++;
        for(volatile unsigned long i = 0; i < COMMON_DELAY; ++ i);
        thread_pcp_bi_sem_signal(&rt_sem_b);

        printf(ANSI_COLOR_GREEN "========rt thread %d end=========" ANSI_COLOR_RESET "\n\n", t_data->rtp.id);
        thread_yield();
    }
}

// the code that each thread runs
static int rt_thread_func (void *data)
{
    struct rt_thread_data *t_data = data;
    int thread_num = t_data->t_num;

    thread_sem_post(t_data->sem);
    switch (thread_num) {
        case 0:
            rt_thread_0 (t_data);
            break;
        case 1:
            rt_thread_1 (t_data);
            break;
        case 2:
            rt_thread_2 (t_data);
            break;
    }

    return thread_num;
}

static int idle_func (void *data)
{
    thread_set_id(100);
    unsigned long a = 0;
    for (;;) {
        a ++;
        thread_yield();
    }
    return 0;
}

static void initial_thread_rtp (struct rt_thread_data *rd, unsigned long wcet, unsigned long period, unsigned int id)
{
    rd->rtp.wcet = wcet;
    rd->rtp.period = period;
    rd->rtp.id = id;
}

int main(int argc, char *argv[])
{
    errval_t err;
    int num_threads = 0;

    // # of threads to start
    if (argc == 2) {
        num_threads = atoi(argv[1]);
        debug_printf("starting %d threads\n", num_threads);
    } else {
        printf("usage %s num_threads\n", argv[0]);
        return EXIT_FAILURE;
    }

    // setup lock, counter, and semaphore
    thread_pcp_bi_sem_init(&rt_sem_a, 1);
    thread_pcp_bi_sem_init(&rt_sem_b, 2);
    
    int start_cntr = 0;
    struct thread_sem sem = THREAD_SEM_INITIALIZER;
    thread_sem_init(&sem, 0);

    // set thread argument data
    struct rt_thread_data *t_data;
    t_data = malloc(num_threads*sizeof(struct rt_thread_data));
    assert(t_data != NULL);

    thread_create(idle_func, NULL);

    initial_thread_rtp (&(t_data[0]), 200, 2000, 1001); // the highest priority
    initial_thread_rtp (&(t_data[1]), 200, 3500, 1002); // the middle  priority
    initial_thread_rtp (&(t_data[2]), 200, 5000, 1003); // the lowest  priority
   
    // start threads
    for (int i = 0; i < num_threads; i++) {
        t_data[i].t_num = i;
        t_data[i].start_cntr = &start_cntr;
        t_data[i].sem = &sem;

        t_data[i].t_id = rt_thread_create(rt_thread_func, &(t_data[i]), &(t_data[i].rtp), false);
        if (t_data[i].t_id == NULL) {
            debug_printf("ERROR: starting thread %d\n", i);
        }
        debug_printf("started thread %d\n", i);
    }

    thread_pcp_bi_sem_reg(&rt_sem_a, t_data[0].t_id);
    thread_pcp_bi_sem_reg(&rt_sem_a, t_data[1].t_id);
    thread_pcp_bi_sem_reg(&rt_sem_b, t_data[1].t_id);
    thread_pcp_bi_sem_reg(&rt_sem_b, t_data[2].t_id);
    
    for (int i = 0; i < num_threads; i++) {
        rt_thread_run(t_data[i].t_id);
    }

    // wait until all started
    while (start_cntr != num_threads) {
        thread_yield();
    }

    debug_printf("all threads started\n");

    // wait until all finished
    for (int i = 0; i < num_threads; i++) {
        thread_sem_wait(&sem);
    }

    debug_printf("all threads finished\n");

    // cleanup: join all threads    
    for (int i = 0; i < num_threads; i++) {
        err = thread_join(t_data[i].t_id, &(t_data[i].ret));
        if (err_is_ok(err)) {
            debug_printf("joined thread %d, return value: %d\n", 
                         i, t_data[i].ret);
        } else {
            DEBUG_ERR(err, "in thread_join for thread %d", i);
        }
    }

    debug_printf("finished.\n");

    return EXIT_SUCCESS;
}

