/*
 * \file
 * \brief Kernel scheduling policy: Time Frame Circle (TFC)
 */

/*
 * @auth: dale
 * @date: 2015-04-16
 */

#include <limits.h>
#ifndef SCHEDULER_SIMULATOR
#       include <kernel.h>
#       include <dispatch.h>
#       include <trace/trace.h>
#       include <trace_definitions/trace_defs.h>
#       include <timer.h> // update_sched_timer
#       include <kcb.h>
#endif

#define MAX(a, b)       ((a) > (b) ? (a) : (b))
#define MIN(a, b)       ((a) < (b) ? (a) : (b))


// queue_tail has to be global, as its used from assembly
// this is always kept in sync with kcb_current->queue_tail
struct dcb *queue_tail = NULL;

/// Last (currently) scheduled task, for accounting purposes
static struct dcb *lastdisp = NULL;

/**
 * \brief Returns whether dcb is in scheduling queue.
 * \param dcb   Pointer to DCB to check.
 * \return True if in queue, false otherwise.
 */
static inline bool in_queue(struct dcb *dcb)
{
    return dcb->next != NULL || kcb_current->queue_tail == dcb;
}

static void initiate_task_state (struct dcb *dcb) {
    dcb->duration = 20;
}

static void print_dcb (const char *description, struct dcb *dcb, bool is_print) 
{
    if (is_print) {
        printf("%s : %p %.*s last_dispatch: %lu, type: %d, etime: %lu, duration: %lu\n", 
                description, 
                dcb, DISP_NAME_LEN, 
                ((struct dispatcher_shared_generic*)(dcb->disp))->name,
                dcb->last_dispatch,dcb->type, dcb->etime, dcb->duration);
    }
}

static void print_runnable_queue (void)
{
    int count = 0;
    for (struct dcb* i = kcb_current->queue_head; i; count ++, i = i->next)
        print_dcb("DCB Runnable", i, false);
}

/***
 * @brief: scheduler queue insert
 * @param: [dcb]  point to dcb to insert
 * @param: [head] if true head insert, tail otherwise 
 */
static void queue_insert(struct dcb *dcb, bool head)
{
    if(in_queue(dcb)) {
        return;
    }
 
    print_dcb("tfc runnable queue insert DCB", dcb, false);

    // fresh tfc_major_time_frame
    // kcb_current->tfc_major_time_frame += dcb->duration;
    // printf("tfc major time frame: %lu\n", kcb_current->tfc_major_time_frame);
   
    // Empty queue case
    if(kcb_current->queue_head == NULL) {
        assert(kcb_current->queue_tail == NULL);
        kcb_current->queue_head = kcb_current->queue_tail = queue_tail = dcb;
        return;
    }

    if (head) {
        dcb->next = kcb_current->queue_head;
        kcb_current->queue_head = dcb;
        print_runnable_queue();
    }
    else{
        kcb_current->queue_tail->next = dcb;
        kcb_current->queue_tail = queue_tail = dcb;
    }

}

/**
 * \brief Remove 'dcb' from scheduler ring.
 *
 * Removes dispatcher 'dcb' from the scheduler ring. If it was not in
 * the ring, this function is a no-op. The postcondition for this
 * function is that dcb is not in the ring.
 *
 * \param dcb   Pointer to DCB to remove.
 */
static void queue_remove(struct dcb *dcb)
{
    // No-op if not in scheduler ring
    if(!in_queue(dcb)) {
        return;
    }
    
    print_dcb("queue remove DCB", dcb, false);
    // fresh tfc_major_time_frame
    // kcb_current->tfc_major_time_frame -= dcb->duration;
   
    if(dcb == kcb_current->queue_head) {
        kcb_current->queue_head = dcb->next;
        if(kcb_current->queue_head == NULL) {
            kcb_current->queue_tail = queue_tail =  NULL;
        }

        goto out;
    }

    for(struct dcb *i = kcb_current->queue_head; i != NULL; i = i->next) {
        if(i->next == dcb) {
            i->next = i->next->next;
            if(kcb_current->queue_tail == dcb) {
                kcb_current->queue_tail = queue_tail = i;
            }
            break;
        }
    }

 out:
    dcb->next = NULL;
}

void make_runnable(struct dcb *dcb)
{
    struct dispatcher_shared_generic *disp 
        = get_dispatcher_shared_generic(dcb->disp);
    // No-Op if already in schedule
    if(in_queue(dcb)) {
        return;
    }
    
    initiate_task_state(dcb);
    dcb->etime = 0;

    dcb->type = disp->dcb_type;
    
    if (dcb->type == TASK_TYPE_BEST_EFFORT) {
        queue_insert(dcb, false);
        if (kcb_current->non_rt_queue_head == NULL)
            kcb_current->non_rt_queue_head = dcb;
    }
    else {
        print_dcb("real time dcb: ", dcb, false);
        queue_insert(dcb, true);
    }
}

/**
 * \brief Scheduler policy.
 *
 * \return Next DCB to schedule or NULL if wait for interrupts.
 */
struct dcb *schedule(void)
{
    struct dcb *todisp;

    // printf("schedule, kernel_now: %lu\n", kernel_now);
    // print_runnable_queue();
    if (lastdisp != NULL) {
        //todisp = lastdisp->next == NULL ? 
        //    kcb_current->queue_head : lastdisp->next;
        if (kernel_now%DEFAULT_TFC_MTF == 0) {
            // return queue head
            if (lastdisp != NULL) {
                if (lastdisp->type != TASK_TYPE_BEST_EFFORT) {
                    lastdisp->etime = kernel_now - lastdisp->last_dispatch;
                }
            }
            todisp = kcb_current->queue_head;
            todisp->last_dispatch = kernel_now;
        }
        else  {
            if (lastdisp->type == TASK_TYPE_BEST_EFFORT) {
                todisp = lastdisp->next == NULL ?
                    kcb_current->non_rt_queue_head : lastdisp->next;    
                if (todisp != NULL)
                    todisp->last_dispatch = kernel_now;
            }
            else {
                // real time dcb
                lastdisp->etime = kernel_now - lastdisp->last_dispatch;

                if (lastdisp->etime == lastdisp->duration) {
                    // jump to next dispatcher
                    lastdisp->etime = 0;
                    todisp = lastdisp->next;

                    if (todisp != NULL)
                        todisp->last_dispatch = kernel_now;
                }
                else {
                    todisp = lastdisp;
                }
            }
        }
    }
    else {
        todisp = kcb_current->queue_head;
        todisp->last_dispatch = kernel_now;
    }

    if (todisp != NULL) {
        if (lastdisp != NULL)
            print_dcb("from DCB", lastdisp, false);

        print_dcb("to DCB", todisp, false);
        if (todisp->etime > 20)
            printf("!BING");
    }
    
    lastdisp = todisp;
    return todisp;
}

/**
 * \brief Remove 'dcb' from scheduler ring.
 *
 * Removes dispatcher 'dcb' from the scheduler ring. If it was not in
 * the ring, this function is a no-op. The postcondition for this
 * function is that dcb is not in the ring.
 *
 * \param dcb   Pointer to DCB to remove.
 */
void scheduler_remove(struct dcb *dcb)
{
    // No-Op if not in schedule
    if(!in_queue(dcb)) {
        return;
    }

    if (dcb->type == TASK_TYPE_BEST_EFFORT) {
        if (dcb == kcb_current->non_rt_queue_head) 
            kcb_current->non_rt_queue_head = dcb->next;

        queue_remove(dcb);
    } 
    else {
        // print_dcb("scheduler remove DCB", dcb, true);
    }

    trace_event(TRACE_SUBSYS_KERNEL, TRACE_EVENT_KERNEL_SCHED_REMOVE,
                (uint32_t)(lvaddr_t)dcb & 0xFFFFFFFF);
}

/**
 * \brief Yield 'dcb' for the rest of the current timeslice.
 *
 * Re-sorts 'dcb' into the scheduler queue with its release time increased by
 * the timeslice period. It is an error to yield a dispatcher not in the
 * scheduler queue.
 *
 * \param dcb   Pointer to DCB to remove.
 */
void scheduler_yield(struct dcb *dcb)
{
    print_dcb("Yield DCB", dcb, false);
    if (!in_queue(dcb))
        return;

    if (dcb->type == TASK_TYPE_BEST_EFFORT) {
        // no op
    }
    else {
        // printf("we don't kown how to yield real time dcb.\n");
    }
}

#ifndef SCHEDULER_SIMULATOR
void scheduler_reset_time(void)
{
    trace_event(TRACE_SUBSYS_KERNEL, TRACE_EVENT_KERNEL_TIMER_SYNC, 0);
    kernel_now = 0;

    // XXX: Currently, we just re-release everything now
    struct kcb *k = kcb_current;
    do {
        printk(LOG_NOTE, "clearing kcb %p\n", k);
        for(struct dcb *i = k->queue_head; i != NULL; i = i->next) {
            i->etime = 0;
            i->last_dispatch = 0;
        }
        k = k->next;
    }while(k && k!=kcb_current);

    // Forget all accounting information
    lastdisp = NULL;
}

void scheduler_convert(void)
{
    panic("tfc scheduler don't know how to convert");
}

void scheduler_restore_state(void)
{
    // config tfc scheduler
    // config_tfc_scheduler();
    // clear time slices
   scheduler_reset_time();
    // initial tfc task queue
    // scheduler_initial_queue();
}
#endif
