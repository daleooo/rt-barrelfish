/**
 * \file
 * \brief Thread synchronisation primitives.
 */

/*
 * Copyright (c) 2007, 2008, 2009, 2010, 2012, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Haldeneggsteig 4, CH-8092 Zurich. Attn: Systems Group.
 */

#include <barrelfish/barrelfish.h>
#include <barrelfish/dispatch.h>
#include <barrelfish/dispatcher_arch.h>
#include <trace/trace.h>
#include <trace_definitions/trace_defs.h>
#include "threads_priv.h"

#ifndef TRACE_THREADS
#define trace_event(a,b,c) ((void)0)
#endif

struct srp_saved *srp_stack = NULL;
unsigned int srp_stack_sp = 0;

/**
 * \brief Initialise a condition variable
 *
 * \param cond Condition variable pointer
 */
void thread_cond_init(struct thread_cond *cond)
{
    cond->queue = NULL;
    cond->lock = 0;
}

/**
 * \brief Wait for a condition variable
 *
 * This function waits for the given condition variable to be signalled
 * (through thread_cond_signal() or thread_cond_broadcast()) before
 * returning, while atomically unlocking the mutex pointed to by
 * 'mutex'.
 *
 * \param cond  Condition variable pointer
 * \param mutex Optional pointer to mutex to unlock.
 */
void thread_cond_wait(struct thread_cond *cond, struct thread_mutex *mutex)
{
    dispatcher_handle_t disp = disp_disable();

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_COND_WAIT_ENTER,
                (uintptr_t)cond);

    acquire_spinlock(&cond->lock);

    // Release the lock
    if (mutex != NULL) {
        struct thread *wakeup = thread_mutex_unlock_disabled(disp, mutex);

        if(wakeup != NULL) {
            errval_t err = domain_wakeup_on_disabled(wakeup->disp, wakeup, disp);
            assert_disabled(err_is_ok(err));
        }
    }

    // Block on the condition variable and release spinlock
    thread_block_and_release_spinlock_disabled(disp, &cond->queue, &cond->lock);

    // Re-acquire the mutex
    if (mutex != NULL) {
        thread_mutex_lock(mutex);
    }

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_COND_WAIT_LEAVE,
                (uintptr_t)cond);
}

/**
 * \brief Signal a condition variable
 *
 * This function signals the condition variable, and wakes up one
 * thread waiting on it.
 *
 * \param cond Condition variable pointer
 */
void thread_cond_signal(struct thread_cond *cond)
{
    struct thread *wakeup = NULL;
    errval_t err = SYS_ERR_OK;

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_COND_SIGNAL,
                (uintptr_t)cond);

    // Wakeup one waiting thread
    dispatcher_handle_t disp = disp_disable();
    acquire_spinlock(&cond->lock);
    if (cond->queue != NULL) {
        wakeup = thread_unblock_one_disabled(disp, &cond->queue, NULL);
        if(wakeup != NULL) {
            err = domain_wakeup_on_disabled(wakeup->disp, wakeup, disp);
        }
    }
    release_spinlock(&cond->lock);
    disp_enable(disp);

    if(err_is_fail(err)) {
        USER_PANIC_ERR(err, "remote wakeup from condition signal");
    }

    if(wakeup != NULL) {
        // XXX: Need directed yield to inter-disp thread
        thread_yield();
    }
}

/**
 * \brief Broadcast signal a condition variable
 *
 * This function signals the condition variable, and wakes up all
 * threads waiting on it.
 *
 * \param cond Condition variable pointer
 */
void thread_cond_broadcast(struct thread_cond *cond)
{
    struct thread *wakeupq = NULL;
    bool foreignwakeup = false;

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_COND_BROADCAST,
                (uintptr_t)cond);

    // Wakeup all waiting threads
    dispatcher_handle_t disp = disp_disable();
    acquire_spinlock(&cond->lock);
    wakeupq = thread_unblock_all_disabled(disp, &cond->queue, NULL);
    release_spinlock(&cond->lock);
    disp_enable(disp);

    foreignwakeup = (wakeupq != NULL);
    // Now, wakeup all on foreign dispatchers
    while (wakeupq != NULL) {
        struct thread *wakeup = wakeupq;
        wakeupq = wakeupq->next;
        errval_t err = domain_wakeup_on(wakeup->disp, wakeup);
        if(err_is_fail(err)) {
            USER_PANIC_ERR(err, "remote wakeup from condition broadcast");
        }
    }

    if(foreignwakeup) {
        // XXX: Need directed yield to inter-disp thread
        thread_yield();
    }
}

/**
 * \brief Initialise a mutex
 *
 * \param mutex Mutex pointer
 */
void thread_mutex_init(struct thread_mutex *mutex)
{
    mutex->locked = 0;
    mutex->holder = NULL;
    mutex->queue = NULL;
    mutex->lock = 0;
}

/**
 * \brief Lock a mutex
 *
 * This blocks until the given mutex is unlocked, and then atomically locks it.
 *
 * \param mutex Mutex pointer
 */
void thread_mutex_lock(struct thread_mutex *mutex)
{
    dispatcher_handle_t handle = disp_disable();
    struct dispatcher_generic *disp_gen = get_dispatcher_generic(handle);

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_MUTEX_LOCK_ENTER,
                (uintptr_t)mutex);

    acquire_spinlock(&mutex->lock);
    if (mutex->locked > 0) {
        thread_block_and_release_spinlock_disabled(handle, &mutex->queue,
                                                   &mutex->lock);
    } else {
        mutex->locked = 1;
        mutex->holder = disp_gen->current;
        release_spinlock(&mutex->lock);
        disp_enable(handle);
    }

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_MUTEX_LOCK_LEAVE,
                (uintptr_t)mutex);
}

void nonrt_thread_mutex_lock(struct thread_mutex *mutex)
{
    dispatcher_handle_t handle = disp_disable();
    struct dispatcher_generic *disp_gen = get_dispatcher_generic(handle);
    struct dispatcher_shared_generic *disp = get_dispatcher_shared_generic(handle);

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_MUTEX_LOCK_ENTER,
                (uintptr_t)mutex);

    acquire_spinlock(&mutex->lock);
    if (mutex->locked > 0) {
        debug_printf("waiting for %d\n", (int)mutex->holder->id);
        nonrt_thread_block_and_release_spinlock_disabled(handle, &mutex->queue,
                                                   &mutex->lock);
    } else {
        debug_printf("thread %d lock mutex\n", (int)disp_gen->current->id);
        mutex->locked = 1;
        mutex->holder = disp_gen->current;
        disp_gen->current->rt->blocking_time = disp->systime;
        release_spinlock(&mutex->lock);
        disp_enable(handle);
        //debug_printf("thread %d lock mutex end\n", (int)disp_gen->current->id);
    }

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_MUTEX_LOCK_LEAVE,
                (uintptr_t)mutex);
}

/**
 * \brief Lock a mutex
 *
 * This blocks until the given mutex is unlocked, and then atomically locks it.
 *
 * \param mutex Mutex pointer
 */
void thread_mutex_lock_nested(struct thread_mutex *mutex)
{
    dispatcher_handle_t handle = disp_disable();
    struct dispatcher_generic *disp_gen = get_dispatcher_generic(handle);

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_MUTEX_LOCK_NESTED_ENTER,
                (uintptr_t)mutex);

    acquire_spinlock(&mutex->lock);
    if (mutex->locked > 0
        && mutex->holder != disp_gen->current) {
        thread_block_and_release_spinlock_disabled(handle, &mutex->queue,
                                                   &mutex->lock);
    } else {
        mutex->locked++;
        mutex->holder = disp_gen->current;
        release_spinlock(&mutex->lock);
        disp_enable(handle);
    }

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_MUTEX_LOCK_NESTED_LEAVE,
                (uintptr_t)mutex);
}

/**
 * \brief Try to lock a mutex
 *
 * If the given mutex is unlocked, this atomically locks it and returns true,
 * otherwise it returns false immediately.
 *
 * \param mutex Mutex pointer
 *
 * \returns true if lock acquired, false otherwise
 */
bool thread_mutex_trylock(struct thread_mutex *mutex)
{
    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_MUTEX_TRYLOCK,
                (uintptr_t)mutex);

    // Try first to avoid contention
    if (mutex->locked > 0) {
        return false;
    }

    dispatcher_handle_t handle = disp_disable();
    struct dispatcher_generic *disp_gen = get_dispatcher_generic(handle);
    bool ret;

    acquire_spinlock(&mutex->lock);
    if (mutex->locked > 0) {
        ret = false;
    } else {
        ret = true;
        mutex->locked = 1;
        mutex->holder = disp_gen->current;
    }
    release_spinlock(&mutex->lock);

    disp_enable(handle);
    return ret;
}

/**
 * \brief Unlock a mutex, while disabled
 *
 * This function unlocks the given mutex. It may only be called while disabled.
 *
 * \param disp Dispatcher pointer
 * \param mutex Mutex pointer
 *
 * \return Pointer to thread to be woken on foreign dispatcher
 */
struct thread *thread_mutex_unlock_disabled(dispatcher_handle_t handle,
                                            struct thread_mutex *mutex)
{
    struct thread *ft = NULL;

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_MUTEX_UNLOCK,
                (uintptr_t)mutex);

    acquire_spinlock(&mutex->lock);
    assert_disabled(mutex->locked > 0);

    if(mutex->locked == 1) {
        // Wakeup one waiting thread
        if (mutex->queue != NULL) {
            // XXX: This assumes dequeueing is off the top of the queue
            mutex->holder = mutex->queue;
            ft = thread_unblock_one_disabled(handle, &mutex->queue, NULL);
        } else {
            mutex->holder = NULL;
            mutex->locked = 0;
        }
    } else {
        mutex->locked--;
    }

    release_spinlock(&mutex->lock);
    return ft;
}

/**
 * \brief Unlock a mutex
 *
 * This unlocks the given mutex.
 *
 * \param mutex Mutex pointer
 */
void thread_mutex_unlock(struct thread_mutex *mutex)
{
    dispatcher_handle_t disp = disp_disable();
    struct thread *wakeup = thread_mutex_unlock_disabled(disp, mutex);
    errval_t err = SYS_ERR_OK;

    if (wakeup != NULL) {
        err = domain_wakeup_on_disabled(wakeup->disp, wakeup, disp);
    }
    disp_enable(disp);

    if(err_is_fail(err)) {
        USER_PANIC_ERR(err, "remote wakeup from mutex unlock");
    }

    if(wakeup != NULL) {
        // XXX: Need directed yield to inter-disp thread
        thread_yield();
    }
}

void nonrt_thread_mutex_unlock(struct thread_mutex *mutex)
{
    dispatcher_handle_t handle = disp_disable();

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_MUTEX_UNLOCK,
                (uintptr_t)mutex);

    struct dispatcher_generic *disp_gen = get_dispatcher_generic(handle);
    struct dispatcher_shared_generic *disp = get_dispatcher_shared_generic(handle);
    acquire_spinlock(&mutex->lock);
    assert_disabled(mutex->locked > 0);

    debug_printf("thread %d unlock mutex\n", (int)disp_gen->current->id);
    if(mutex->locked == 1) {
        debug_printf("thread %d blocking time: %ld\n", (int)disp_gen->current->id, 
                disp->systime - disp_gen->current->rt->blocking_time);
        // Wakeup one waiting thread
        if (mutex->queue != NULL) {
            // XXX: This assumes dequeueing is off the top of the queue
            mutex->holder = mutex->queue;
            nonrt_thread_unblock_one_and_release_spinlock_disabled(handle, &mutex->queue, &mutex->lock);
            return;
        } else {
            mutex->holder = NULL;
            mutex->locked = 0;
        }
    } else {
        mutex->locked--;
    }

    release_spinlock(&mutex->lock);
    disp_enable(handle);
}

void thread_sem_init(struct thread_sem *sem, unsigned int value)
{
    assert(sem != NULL);

    sem->value = value;
    sem->queue = NULL;
    sem->lock = 0;
}

void thread_sem_wait(struct thread_sem *sem)
{
    assert(sem != NULL);

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_SEM_WAIT_ENTER,
                (uintptr_t)sem);

    dispatcher_handle_t disp = disp_disable();
    acquire_spinlock(&sem->lock);

    if(sem->value < 1) {
        // Not possible to decrement -- wait!
        thread_block_and_release_spinlock_disabled(disp, &sem->queue, &sem->lock);
    } else {
        // Decrement possible
        sem->value--;
        release_spinlock(&sem->lock);
        disp_enable(disp);
    }

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_SEM_WAIT_LEAVE,
                (uintptr_t)sem);
}

bool thread_sem_trywait(struct thread_sem *sem)
{
    assert(sem != NULL);
    bool ret = false;

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_SEM_TRYWAIT,
                (uintptr_t)sem);

    dispatcher_handle_t disp = disp_disable();
    acquire_spinlock(&sem->lock);

    if(sem->value >= 1) {
        // Decrement possible
        sem->value--;
        ret = true;
    }

    release_spinlock(&sem->lock);
    disp_enable(disp);

    return ret;
}

void thread_sem_post(struct thread_sem *sem)
{
    assert(sem != NULL);

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_SEM_POST, (uintptr_t)sem);

    dispatcher_handle_t disp = disp_disable();
    struct thread *wakeup = NULL;
    errval_t err = SYS_ERR_OK;
    acquire_spinlock(&sem->lock);

    // Wakeup one?
    if(sem->value == 0 && sem->queue != NULL) {
        wakeup = thread_unblock_one_disabled(disp, &sem->queue, NULL);
    } else {
        sem->value++;
    }

    if(wakeup != NULL) {
        err = domain_wakeup_on_disabled(wakeup->disp, wakeup, disp);
        assert_disabled(err_is_ok(err));
    }

    release_spinlock(&sem->lock);
    disp_enable(disp);

    if(err_is_fail(err)) {
        USER_PANIC_ERR(err, "remote wakeup from semaphore post");
    }

    if(wakeup != NULL) {
        // XXX: Need directed yield to inter-disp thread
        thread_yield();
    }
}

void thread_pi_bi_sem_init(struct thread_pi_bi_sem *sem, unsigned int id)
{
    assert(sem != NULL);

    sem->id    = id;
    sem->value = 1; // binary semaphore
    sem->lock  = 0;
    sem->holder= NULL;
    sem->queue = NULL;
}

static void thread_pi_bi_sem_insert_to_queue(struct thread_pi_bi_sem **head,
                                           struct thread_pi_bi_sem **tail,  
                                           struct thread_pi_bi_sem *sem)
{
    if (sem->blocked_next != NULL) 
        return;  // already in head

    // insert to the first
    if (*head == NULL) {
        *head = *tail = sem;
        sem->blocked_next = *head;
    }
    else {
        // head insert
        sem->blocked_next = (*head)->blocked_next;
        (*tail)->blocked_next = *head = sem;
    }
}

static void thread_pi_bi_sem_remove_from_queue(struct thread_pi_bi_sem **head,
                                             struct thread_pi_bi_sem **tail,  
                                             struct thread_pi_bi_sem *sem)
{
    if (sem->blocked_next == NULL)
        return;

    // remove first
    if (sem == *head) {
        // last one
        if (sem == *tail)
            *head = *tail = NULL;
        else
            (*tail)->blocked_next = (*head = sem->blocked_next);
    }
    else {
        struct thread_pi_bi_sem *s;
        for (s = *head; s->blocked_next != *head && 
                s->blocked_next != sem; s = s->blocked_next) {} 
        assert_disabled(s->blocked_next != *head);

        s->blocked_next = sem->blocked_next;
        if (sem == *tail)
            *tail = s;
    }
    sem->blocked_next = NULL;
}


void thread_pi_bi_sem_wait(struct thread_pi_bi_sem *sem)
{
    assert(sem != NULL);

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_SEM_WAIT_ENTER,
                (uintptr_t)sem);

    dispatcher_handle_t handle = disp_disable();
    struct dispatcher_shared_generic *disp = get_dispatcher_shared_generic(handle);
    struct dispatcher_generic *disp_gen = get_dispatcher_generic(handle);
    acquire_spinlock(&sem->lock);
    struct thread *me = disp_gen->current;
    
    if(sem->value < 1) {
        // Not possible to decrement -- wait!
        me->rt->blocked_time = disp->systime;
        debug_printf("waiting for %d guarded by sem %d\n", (int)sem->holder->id, sem->id);
        me->rt->pi_lock = sem;
         
        struct thread *t = sem->holder;
        
        assert_disabled(me->rt->priority < t->rt->priority);
        while (t->rt->pi_lock != NULL) {
            assert_disabled(t->rt->pi_lock->holder != NULL);
            t->rt->priority = me->rt->priority;
            // update priority lead to update sem queue
            thread_remove_from_queue(&t->rt->pi_lock->queue, t);
            rt_thread_insert_to_queue_by_priority(&t->rt->pi_lock->queue, t);
            t = t->rt->pi_lock->holder;
        }

        t->rt->priority = me->rt->priority;

        debug_printf("pi: thread %d priority change to %ld\n", (int)t->id, t->rt->priority);
        rt_thread_pi_block_and_release_spinlock_disabled(handle, &sem->queue, &sem->lock);
    } else {
        // Decrement possible
        debug_printf("thread %d enter critical zone guarded by sem %d\n", (int)me->id, sem->id);
        sem->value--;
        sem->holder = me;
        me->rt->blocking_time = disp->systime;
        thread_pi_bi_sem_insert_to_queue(&disp_gen->locked_pi_bi_sems_head, 
                &disp_gen->locked_pi_bi_sems_tail, sem);
        release_spinlock(&sem->lock);
        disp_enable(handle);
    }

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_SEM_WAIT_LEAVE,
                (uintptr_t)sem);
}

static unsigned long thread_current_pi_priority(dispatcher_handle_t handle, struct thread *me)
{
    struct dispatcher_generic *disp_gen = get_dispatcher_generic(handle);
    struct thread_pi_bi_sem *pi_sem = disp_gen->locked_pi_bi_sems_head;
    unsigned long priority = rt_thread_priority(me);

    while (pi_sem) {
       
        // hold by me and someone is waiting
        if (pi_sem->holder == me && pi_sem->queue != NULL) {
            assert_disabled(priority > pi_sem->queue->rt->priority);
            priority = pi_sem->queue->rt->priority;
        }
        
        if (pi_sem == disp_gen->locked_pi_bi_sems_tail)
            break;
        pi_sem = pi_sem->blocked_next;
    }

    return priority;
}

void thread_pi_bi_sem_signal(struct thread_pi_bi_sem *sem)
{
    assert(sem != NULL);

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_SEM_POST, (uintptr_t)sem);

    dispatcher_handle_t handle = disp_disable();
    // struct dispatcher_shared_generic *disp = get_dispatcher_shared_generic(handle);
    struct dispatcher_generic *disp_gen = get_dispatcher_generic(handle);
    acquire_spinlock(&sem->lock);
    
    struct thread *me = disp_gen->current;

    // Wakeup one?
    debug_printf("thread %d signal sem %d\n", (int)me->id, sem->id);
    if(sem->value == 0 && sem->queue != NULL) {

        struct thread *wakeup = rt_thread_pi_unblock_one_disabled(handle, &sem->queue);
        assert_disabled(wakeup != NULL);
        
        // update priority
        me->rt->priority = thread_current_pi_priority(handle, me);
        debug_printf("pi: thread %d change priority to %ld\n", (int)me->id, me->rt->priority);
        
        sem->holder = wakeup;
        wakeup->rt->pi_lock = NULL;
        rt_thread_schedule_imme_and_release_spinlock_disabled(handle, &sem->lock);
    } else {
        sem->holder = NULL;
        sem->value++;
        thread_pi_bi_sem_remove_from_queue(&disp_gen->locked_pi_bi_sems_head, &disp_gen->locked_pi_bi_sems_tail,
                sem);
        release_spinlock(&sem->lock);
        disp_enable(handle);
    }
}

void thread_update_pcp_sem_ceiling_disabled(dispatcher_handle_t handle, struct thread *me)
{
    struct dispatcher_shared_generic *disp = get_dispatcher_shared_generic(handle);

    struct thread_pcp_bi_sem **sem = me->rt->pcp_sem_list;
    for (int i = 0; sem && i < THREAD_MAX_PCP_SEM_NUM && sem[i] != NULL; ++ i) {
        acquire_spinlock(&sem[i]->lock);
        if (sem[i]->ceiling == 0)
            sem[i]->ceiling = me->rt->priority;
        else {
            if (disp->systime < me->rt->priority && 
                    me->rt->priority < sem[i]->ceiling) {
                sem[i]->ceiling = me->rt->priority;
                debug_printf("update sem %d pcp ceiling to %ld\n", 
                        sem[i]->id, sem[i]->ceiling);
            }
        }
        release_spinlock(&sem[i]->lock);
    }
    return;
}

void thread_pcp_bi_sem_init(struct thread_pcp_bi_sem *sem, unsigned int id)
{
    assert(sem != NULL);

    sem->id = id;
    sem->value = 1;
    sem->lock = 0;

    sem->ceiling = 0;
    sem->holder  = NULL;
    sem->blocked_next = NULL;
}

void thread_pcp_bi_sem_reg(struct thread_pcp_bi_sem *sem, struct thread *thread) 
{
    assert(sem != NULL);
    
    dispatcher_handle_t handle = disp_disable();
    acquire_spinlock(&sem->lock);

    long int priority = thread->rt->priority;

    if (sem->ceiling == 0 || priority < sem->ceiling)  {
        sem->ceiling = priority;   
        debug_printf("update sem %d ceiling to %ld\n", sem->id, sem->ceiling);
    }   

    release_spinlock(&sem->lock);
    disp_enable(handle);
}

void thread_dpcp_bi_sem_reg(struct thread_pcp_bi_sem **sem, struct thread *thread) 
{
    assert(sem != NULL);
   
    dispatcher_handle_t handle = disp_disable();
    thread->rt->pcp_sem_list = sem; 
    disp_enable(handle);
}

static void thread_pcp_bi_sem_insert_to_queue(struct thread_pcp_bi_sem **head,
                                           struct thread_pcp_bi_sem **tail,  
                                           struct thread_pcp_bi_sem *sem)
{
    if (sem->blocked_next != NULL) 
        return;  // already in head

    // insert to the first
    if (*head == NULL) {
        *head = *tail = sem;
        sem->blocked_next = *head;
    }
    else {
        if (sem->ceiling <= (*head)->ceiling) {
            sem->blocked_next = *head;
            *head = (*tail)->blocked_next = sem;
        }
        else if (sem->ceiling >= (*tail)->ceiling) {
            *tail = ((*tail)->blocked_next = sem);
            sem->blocked_next = *head;
        }
        else { struct thread_pcp_bi_sem *s;
            for(s = *head; s->blocked_next != *head
                    && s->blocked_next->ceiling < sem->ceiling; s = s->blocked_next) {}
            sem->blocked_next = s->blocked_next;
            s->blocked_next = sem; 
        }
    }
}

static void thread_pcp_bi_sem_remove_from_queue(struct thread_pcp_bi_sem **head,
                                             struct thread_pcp_bi_sem **tail,  
                                             struct thread_pcp_bi_sem *sem)
{
    if (sem->blocked_next == NULL)
        return;

    // remove first
    if (sem == *head) {
        // last one
        if (sem == *tail)
            *head = *tail = NULL;
        else
            (*tail)->blocked_next = (*head = sem->blocked_next);
    }
    else {
        struct thread_pcp_bi_sem *s;
        for (s = *head; s->blocked_next != *head && 
                s->blocked_next != sem; s = s->blocked_next) {} 
        assert_disabled(s->blocked_next != *head);

        s->blocked_next = sem->blocked_next;
        if (sem == *tail)
            *tail = s;
    }
    sem->blocked_next = NULL;
}

void thread_pcp_bi_sem_signal(struct thread_pcp_bi_sem *sem)
{
    assert(sem != NULL);

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_SEM_POST, (uintptr_t)sem);

    dispatcher_handle_t handle = disp_disable();
    struct dispatcher_generic *disp_gen = get_dispatcher_generic(handle);
    
    acquire_spinlock(&sem->lock);
    struct thread *me = disp_gen->current;

    assert_disabled(me == sem->holder);
    
    debug_printf("thread %d signal sem %d\n", (int)me->id, sem->id);
    if(sem->value == 0 && sem->queue != NULL) {
        
        unsigned long priority = me->rt->priority;
        // update priority
        struct thread_pcp_bi_sem *head = disp_gen->locked_pcp_bi_sems_head;
        struct thread_pcp_bi_sem *pcp_sem;
        for (pcp_sem = sem->blocked_next; pcp_sem != head; pcp_sem = pcp_sem->blocked_next) {
            if (pcp_sem->holder == me)
                break;  
        }
        if (pcp_sem == head) {
            // no other thread blocked by me
            me->rt->priority = rt_thread_priority(me);
        }
        else {
            // someone blocked by me
            assert_disabled(pcp_sem->queue != NULL);
            assert_disabled(pcp_sem->queue->rt->priority < me->rt->priority);
            me->rt->priority = pcp_sem->queue->rt->priority;
        }
        debug_printf("pcp: thread %d change priority to %ld\n", (int)me->id, me->rt->priority);
        
        struct thread *wakeup = rt_thread_pcp_unblock_one_disabled(handle, &sem->queue);
        
        assert_disabled(wakeup != NULL);
        
        assert_disabled(wakeup->rt->priority >= priority);
        wakeup->rt->priority = priority;
        debug_printf("pcp: thread %d change priority to %ld\n", (int)wakeup->id, wakeup->rt->priority);
        
        sem->holder = wakeup;
        rt_thread_schedule_imme_and_release_spinlock_disabled(handle, &sem->lock);
    }
    else {
        sem->value ++;
        thread_pcp_bi_sem_remove_from_queue(&disp_gen->locked_pcp_bi_sems_head,
                &disp_gen->locked_pcp_bi_sems_tail, sem);
        sem->holder = NULL;
        release_spinlock(&sem->lock);
        disp_enable(handle);
    }
}
/*void thread_pcp_bi_sem_wait(struct thread_pcp_bi_sem *sem)
{
    assert(sem != NULL);

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_SEM_WAIT_ENTER,
                (uintptr_t)sem);

    dispatcher_handle_t handle = disp_disable();
    struct dispatcher_shared_generic *disp = get_dispatcher_shared_generic(handle);
    struct dispatcher_generic *disp_gen = get_dispatcher_generic(handle);
    
    acquire_spinlock(&sem->lock);
    struct thread *me = disp_gen->current;


}*/

void thread_pcp_bi_sem_wait(struct thread_pcp_bi_sem *sem)
{
    assert(sem != NULL);

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_SEM_WAIT_ENTER,
                (uintptr_t)sem);

    dispatcher_handle_t handle = disp_disable();
    struct dispatcher_shared_generic *disp = get_dispatcher_shared_generic(handle);
    struct dispatcher_generic *disp_gen = get_dispatcher_generic(handle);
    
    acquire_spinlock(&sem->lock);
    struct thread *me = disp_gen->current;
    long int priority = me->rt->priority;
    long int ceiling = -1;
    
    // find sem which is not block by me and its ceiling is the highest
    struct thread_pcp_bi_sem *pcp_sem;
    struct thread_pcp_bi_sem *head = disp_gen->locked_pcp_bi_sems_head;
    for (pcp_sem = head; pcp_sem && pcp_sem->holder == me && pcp_sem->blocked_next != head; 
            pcp_sem = pcp_sem->blocked_next) {}
    if(head != NULL)
        debug_printf("head is not null\n");
    
    if (pcp_sem != NULL && pcp_sem->holder != me) {
        ceiling = pcp_sem->ceiling;
    }
    
    if (sem->value < 1 || (ceiling > 0 && priority >= ceiling)) {
        debug_printf("waiting for %d\n", (int)sem->holder->id);
        
        me->rt->blocked_time = disp->systime;
        me->rt->pcp_lock = sem;

        struct thread *t = sem->holder;

        assert_disabled(me->rt->priority < t->rt->priority);
        t->rt->priority = me->rt->priority;
        debug_printf("pcp: thread %d priority change to %ld\n", (int)t->id, t->rt->priority);
        
        rt_thread_pcp_block_and_release_spinlock_disabled(handle, &sem->queue, &sem->lock);
    }
    else {
        // there is no blocked semaphore or priority > ceiling
        debug_printf("thread %d enter critical zone\n", (int)me->id);
        sem->value --;
        sem->holder = me;
        me->rt->blocking_time = disp->systime;
        thread_pcp_bi_sem_insert_to_queue(&disp_gen->locked_pcp_bi_sems_head, &disp_gen->locked_pcp_bi_sems_tail, sem);
        release_spinlock(&sem->lock);
        disp_enable(handle);
    }
    
    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_SEM_WAIT_LEAVE,
                (uintptr_t)sem);
}

void thread_srp_sem_init(struct thread_srp_sem *sem, unsigned int value, unsigned int id)
{
    assert(sem != NULL);
    assert(value <= SRP_SEM_MAX_R_NUM);

    if (srp_stack == NULL)
        srp_stack = malloc(MAX_SRP_STACK_LENGTH*sizeof(struct srp_saved));

    sem->value = value;
    sem->avail = value;
    sem->id = id;
    sem->lock = 0;
    memset((char *)sem->ceiling, 0, (SRP_SEM_MAX_R_NUM + 1)*sizeof(unsigned long));
}


void thread_srp_sem_reg(struct thread_srp_sem *sem, struct thread *thread, unsigned int max_require_num) 
{
    assert(sem != NULL);
    assert(max_require_num <= sem->value);

    // update ceiling for sem
    for (int i = 0; i < sem->value; ++ i) {
        if (sem->ceiling[i] == 0 || sem->ceiling[i] > thread->rt->preem) {
            sem->ceiling[i] = thread->rt->preem;
            debug_printf("update srp sem %d ceiling to %ld, available resource %d\n", 
                    sem->id, (sem->ceiling)[i], i);
        }
    }
}

void thread_srp_print_resource_ceiling(struct thread_srp_sem *sem)
{
    debug_printf("srp sem %d\n", sem->id);
    for (int i = 0; i <= SRP_SEM_MAX_R_NUM; ++ i) {
        debug_printf("available resource %d: %ld\n", i, (sem->ceiling)[i]);
    }
}

static void thread_srp_stack_push_disabled(struct srp_saved *saved) 
{
    assert_disabled(srp_stack_sp < MAX_SRP_STACK_LENGTH); 
    srp_stack[srp_stack_sp] = *saved;
    srp_stack_sp ++;
}

static struct srp_saved thread_srp_stack_pop_disabled(void)
{
    srp_stack_sp --;
    assert_disabled(srp_stack_sp >= 0);
    return srp_stack[srp_stack_sp];
}

void thread_srp_sem_wait(struct thread_srp_sem *sem, unsigned int require_num)
{
    assert(sem != NULL);

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_SEM_WAIT_ENTER,
                (uintptr_t)sem);

    dispatcher_handle_t handle = disp_disable();
    //struct dispatcher_shared_generic *disp = get_dispatcher_shared_generic(handle);
    struct dispatcher_generic *disp_gen = get_dispatcher_generic(handle);
    acquire_spinlock(&sem->lock);
    struct thread *me = disp_gen->current;
    
    debug_printf("thread %d enter critical zone guarded by sem %d\n", (int)me->id, sem->id);
    
    struct srp_saved saved;
    saved.avail_num   = sem->avail;
    saved.sys_ceiling = disp_gen->srp_sys_ceiling;
    saved.thread = me;
    saved.sem    = sem;
    saved.require_num = require_num;

    sem->avail -= require_num;
    if (sem->ceiling[sem->avail] < disp_gen->srp_sys_ceiling || disp_gen->srp_sys_ceiling == 0) {
        disp_gen->srp_sys_ceiling = sem->ceiling[sem->avail];
        debug_printf("system ceiling update to %ld\n", disp_gen->srp_sys_ceiling);
    }
    
    thread_srp_stack_push_disabled(&saved);
    
    release_spinlock(&sem->lock);
    disp_enable(handle);

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_SEM_WAIT_LEAVE,
                (uintptr_t)sem);
}

void thread_srp_sem_signal(struct thread_srp_sem *sem)
{
    assert(sem != NULL);

    trace_event(TRACE_SUBSYS_THREADS, TRACE_EVENT_THREADS_SEM_POST, (uintptr_t)sem);

    dispatcher_handle_t handle = disp_disable();
    // struct dispatcher_shared_generic *disp = get_dispatcher_shared_generic(handle);
    struct dispatcher_generic *disp_gen = get_dispatcher_generic(handle);
    acquire_spinlock(&sem->lock);
    
    struct thread *me = disp_gen->current;
    debug_printf("thread %d exit critical zone guarded by sem %d\n", (int)me->id, sem->id);
    struct srp_saved saved = thread_srp_stack_pop_disabled();
    sem->avail = saved.avail_num;
    
    unsigned long prev_sys_ceiling = disp_gen->srp_sys_ceiling;
    disp_gen->srp_sys_ceiling = saved.sys_ceiling;
    debug_printf("system ceiling update to %ld\n", disp_gen->srp_sys_ceiling);
    if (prev_sys_ceiling < saved.sys_ceiling || saved.sys_ceiling == 0) {
        rt_thread_schedule_imme_and_release_spinlock_disabled(handle, &sem->lock);
    }
    else {
        release_spinlock(&sem->lock);
        disp_enable(handle);
    }
}
